#!/usr/bin/python3

import os
import sys
import tempfile
import subprocess
from pathlib import Path

print("yo")

root = Path(os.path.realpath(__file__)).parent
temp = Path(tempfile.gettempdir())

__VERSION__ = "1.0.0"
__SOURCE__  = "https://gitlab.com/mocchapi/lpp3-check"


def main():
	with open(root / 'api.lua', 'r') as f:
		api_text = f.read()
	api_text += '\n\n\n'
	line_count = api_text.count('\n')

	file = Path(sys.argv[1])

	with open(file,'r') as f:
		file_text = f.read()

	with open(temp / file.name, 'w') as f:
		f.write(api_text + file_text)

	# output = subprocess.run(['luacheck', temp / file.name, '--no-unused', '--no-unused-args', '--ignore','112', '--ignore','631', '--ignore','612'] + sys.argv[2:], capture_output=True)
	output = subprocess.run(['luacheck', temp / file.name, '--no-unused', '--no-unused-args', '--ignore','631', '--ignore','612', '--ignore','611', '--ignore','613', '--ignore','614'] + sys.argv[2:], capture_output=True)
	# print(vars(output))

	checkout = output.stdout.decode('utf-8')
	out = []
	if file.name+':' in checkout:
		for line in checkout.split('\n'):
			if file.name+':' in line:
				first,line_number,*rest =line.split(':')

				line = first+':'+str(int(line_number)-line_count)+':'+':'.join(rest)
			out.append(line)

	print('\n'.join(out))

if __name__ == '__main__':
	main()