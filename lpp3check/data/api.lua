--
-- Dummy functions & globals for static checking & such
--

-- start of Environment Variables

local TOP_SCREEN = 0
 -- Top Screen Image_ID for Screen functions.

local BOTTOM_SCREEN = 0
 -- Bottom Screen Image_ID for Screen functions.

local LEFT_EYE = 0
 -- ID to print for left eye in stereoscopic 3D.

local RIGHT_EYE = 0
 -- ID to print for right eye in stereoscopic 3D.

local KEY_A = 0
 -- Key A value in u32 format for Controls functions.

local KEY_B = 0
 -- Key B value in u32 format for Controls functions.

local KEY_R = 0
 -- Key R value in u32 format for Controls functions.

local KEY_L = 0
 -- Key L value in u32 format for Controls functions.

local KEY_START = 0
 -- Key START value in u32 format for Controls functions.

local KEY_SELECT = 0
 -- Key SELECT value in u32 format for Controls functions.

local KEY_X = 0
 -- Key X value in u32 format for Controls functions.

local KEY_Y = 0
 -- Key X value in u32 format for Controls functions.

local KEY_ZL = 0
 -- Key ZL value in u32 format for Controls functions.

local KEY_ZR = 0
 -- Key ZR value in u32 format for Controls functions.

local KEY_DRIGHT = 0
 -- Key Digital Right value in u32 format for Controls functions.

local KEY_DLEFT = 0
 -- Key Digital Left value in u32 format for Controls functions.

local KEY_DUP = 0
 -- Key Digital Up value in u32 format for Controls functions.

local KEY_DDOWN = 0
 -- Key Digital Down value in u32 format for Controls functions.

local KEY_TOUCH = 0
 -- Key Touchscreen value in u32 format for Controls functions.

local KEY_HOME = 0
 -- Key Home value in u32 format for Controls functions.

local KEY_POWER = 0
 -- Key Power value in u32 format for Controls functions.

local FREAD = 0
 -- Attribute for I/O functions for opening a file with Read attribute.

local FWRITE = 0
 -- Attribute for I/O functions for opening a file with Write attribute.

local FCREATE = 0
 -- Attribute for I/O functions for opening a file with Create/Write attributes.

local LOOP = 0
 -- Attribute for Audio/Video functions for looping buffer.

local NO_LOOP = 0
 -- Attribute for Audio/Video functions for no looping buffer.

local NAND = 0
 -- Attribute for CIA NAND managing.

local SDMC = 0
 -- Attribute for CIA SDMC managing.

local APP_RUNNING = 0
 -- Status for homebrew while is not suspended/exiting.

local APP_EXITING = 0
 -- Status for homebrew while is exiting.

local NO_INTERP = 0
 -- No interpolation mode for sounds.

local LINEAR_INTERP = 0
 -- Linear interpolation mode for sounds.

local POLYPHASE_INTERP = 0
 -- Polyphase interpolation mode for sounds.

local INNER_CAM = 0
 -- Inner camera ID.

local OUTER_CAM = 0
 -- Outer camera ID.

local VGA_RES = 0
 -- VGA (640x480) resolution value.

local QVGA_RES = 0
 -- QVGA (320x240) resolution value.

local QQVGA_RES = 0
 -- QVGA (160x120) resolution value.

local CIF_RES = 0
 -- CIF (352x288) resolution value.

local QCIF_RES = 0
 -- QCIF (176x144) resolution value.

local DS_RES = 0
 -- NDS LCD (256x192) resolution value.

local HDS_RES = 0
 -- Hi-Res NDS LCD (512x384) resolution value.

local CTR_RES = 0
 -- 3DS LCD (400x240) resolution value.

local NEW_3DS_CLOCK = 0
 -- 804 MHZ global variable.

local OLD_3DS_CLOCK = 0
 -- 268 MHZ global variable.

local PHOTO_MODE_NORMAL = 0
 -- Normal photo mode state.

local PHOTO_MODE_PORTRAIT = 0
 -- Portrait photo mode state.

local PHOTO_MODE_LANDSCAPE = 0
 -- Landscape photo mode state.

local PHOTO_MODE_NIGHTVIEW = 0
 -- Nightview photo mode state.

local PHOTO_MODE_LETTER = 0
 -- Letter photo mode state.

local CENTER = 0
 -- View ID value for centered viewport.

local BORDER = 0
 -- View ID value for bordered viewport.

local HEAD_METHOD = 0
 -- HEAD method ID for http requests.

local POST_METHOD = 0
 -- POST method ID for http requests.

local GET_METHOD = 0
 -- GET method ID for http requests.

local NOT_PRESSED = 0
 -- Internal state for Keyboard module when no button is pressed.

local PRESSED = 0
 -- Internal state for Keyboard module when a button is pressed.

local CLEANED = 0
 -- Internal state for Keyboard module when clean button is pressed.

local FINISHED = 0
 -- Internal state for Keyboard module when return button is pressed.

-- end of Environment Variables





-- start of io Module

local io = {}

io.open = function(file, attribute, extdata_archive, filesize)
    -- Open a file.

    -- file: string
    -- attribute: int
    -- extdata_archive: u64 (optional)
    -- filesize: u32 (optional)

    -- Returns: s64
end

io.read = function(filestream, offset, size)
    -- Read from an opened file.

    -- filestream: s64
    -- offset: u64
    -- size: u64

    -- Returns: string
end

io.write = function(filestream, offset, text, size)
    -- Write a text in an opened file.

    -- filestream: s64
    -- offset: u64
    -- text: string
    -- size: u64

    -- Returns: void
end

io.size = function(filestream)
    -- Get size of an opened file.

    -- filestream: s64

    -- Returns: u64
end

io.close = function(filestream, extdata_file)
    -- Close an opened file.

    -- filestream: s64
    -- extdata_file: bool (optional)

    -- Returns: void
end

-- end of io Module





-- start of System Module

local System = {}

System.exit = function()
    -- Exit homebrew and returns to Homebrew Menu.

    -- Returns: void
end

System.getFirmware = function()
    -- Returns firmware version.

    -- Returns: u8,u8,u8
end

System.getKernel = function()
    -- Returns kernel version.

    -- Returns: u8,u8,u8
end

System.getRegion = function()
    -- Get console region (1 = USA, 2 = EUR, Other = JPN).

    -- Returns: int
end

System.getUsername = function()
    -- Returns console user username.

    -- Returns: string
end

System.getBirthday = function()
    -- Returns console user birthday.

    -- Returns: u8, u8
end

System.takeScreenshot = function(filename, jpg_compression)
    -- Take a screenshot in BMP/JPG format.

    -- filename: string
    -- jpg_compression: bool (optional)

    -- Returns: void
end

System.checkBuild = function()
    -- Check current working lpp-3ds build type.

    -- Returns: int
end

System.currentDirectory = function(path)
    -- Get or set current directory.

    -- path: string (optional)

    -- Returns: [string]
end

System.deleteFile = function(path, extdata_archive)
    -- Delete a file.

    -- path: string
    -- extdata_archive: u32 (optional)

    -- Returns: void
end

System.deleteDirectory = function(path, extdata_archive)
    -- Delete a directory.

    -- path: string
    -- extdata_archive: u32 (optional)

    -- Returns: void
end

System.renameDirectory = function(path, path2)
    -- Move or rename a directory.

    -- path: string
    -- path2: string

    -- Returns: void
end

System.renameFile = function(path, path2)
    -- Move or rename a file.

    -- path: string
    -- path2: string

    -- Returns: void
end

System.createDirectory = function(path, extdata_archive)
    -- Create a directory.

    -- path: string
    -- extdata_archive: u32 (optional)

    -- Returns: void
end

System.doesFileExist = function(path)
    -- Check if a file exists.

    -- path: string

    -- Returns: bool
end

System.listDirectory = function(path)
    -- List a directory.

    -- path: string

    -- Returns: table
end

System.isBatteryCharging = function()
    -- Check if battery is in charging.

    -- Returns: bool
end

System.getBatteryLife = function()
    -- Get battery life.

    -- Returns: int
end

System.getModel = function()
    -- Get 3DS Model. [Return values: <a href=http://3dbrew.org/wiki/Cfg:GetSystemModel>See here</a>]

    -- Returns: int
end

System.getLanguage = function()
    -- Get 3DS system language.

    -- Returns: int
end

System.launch3DSX = function(filename)
    -- Launch a 3DSX homebrew.

    -- filename: string

    -- Returns: void
end

System.launchPayload = function(filename, offset)
    -- Launch an ARM9 payload.

    -- filename: string
    -- offset: u32

    -- Returns: void
end

System.launchCIA = function(unique_id, mediatype)
    -- Launch an imported CIA.

    -- unique_id: u32
    -- mediatype: u32

    -- Returns: void
end

System.extractSMDH = function(filename)
    -- Extract title, desc, author and icon from a SMDH file.

    -- filename: string

    -- Returns: table
end

System.listExtdataDir = function(path, archive)
    -- List a folder in extdata.

    -- path: string
    -- archive: u64

    -- Returns: table
end

System.scanExtdata = function()
    -- List all extdata folders and files.

    -- Returns: table
end

System.listCIA = function()
    -- List all imported CIA files.

    -- Returns: table
end

System.installCIA = function(filename, mediatype)
    -- Install a CIA file.

    -- filename: string
    -- mediatype: u32

    -- Returns: void
end

System.uninstallCIA = function(delete_id, mediatype)
    -- Uninstall a CIA file.

    -- delete_id: u32
    -- mediatype: u32

    -- Returns: void
end

System.extractFromZIP = function(zip_file, file, path_to_extract, password)
    -- Extract a file from a ZIP file.

    -- zip_file: string
    -- file: string
    -- path_to_extract: string
    -- password: string (optional)

    -- Returns: void
end

System.extractZIP = function(file, path_to_extract, password)
    -- Extract a ZIP file.

    -- file: string
    -- path_to_extract: string
    -- password: string (optional)

    -- Returns: void
end

System.extractCIA = function(file)
    -- Extract ProductCode and UniqueID from a CIA file.

    -- file: string

    -- Returns: void
end

System.checkStatus = function()
    -- Check homebrew status (Like APP_EXITING).

    -- Returns: u8
end

System.showHomeMenu = function()
    -- Sets up necessary syscall for Home/Power buttons support.

    -- Returns: void
end

System.reboot = function()
    -- Reboot Nintendo 3DS.

    -- Returns: void
end

System.launchGamecard = function()
    -- Launch inserted game cartridge.

    -- Returns: void
end

System.getFreeSpace = function()
    -- Get SD card free space.

    -- Returns: u64
end

System.getTime = function()
    -- Get console time.

    -- Returns: int,int,int
end

System.getDate = function()
    -- Get console date.

    -- Returns: int,int,int,int
end

System.getGWRomID = function()
    -- Get Product ID for current loaded rom on a Gateway/Mt Card.

    -- Returns: string
end

System.addNews = function(title, text, image, is_file)
    -- Add a news to official Nintendo News applet.

    -- title: string
    -- text: string
    -- image: image_id/string
    -- is_file: bool

    -- Returns: void
end

System.listNews = function()
    -- List all news from official Nintendo News applet.

    -- Returns: table
end

System.getNews = function(news_id)
    -- Get text from a News.

    -- news_id: u32

    -- Returns: string
end

System.eraseNews = function(news_id)
    -- Erase a News.

    -- news_id: u32

    -- Returns: void
end

System.checkSDMC = function()
    -- Check if SD is inserted.

    -- Returns: bool
end

System.setCpuSpeed = function(value)
    -- Sets CPU frequency.

    -- value: int

    -- Returns: void
end

System.getCpuSpeed = function()
    -- Gets CPU frequency.

    -- Returns: int
end

System.fork = function(mem_block)
    -- Duplicate a generic memory block.

    -- mem_block: int

    -- Returns: int
end

-- end of System Module





-- start of Screen Module

local Screen = {}

Screen.waitVblankStart = function()
    -- Wait Screen Refresh.

    -- Returns: void
end

Screen.flip = function()
    -- Flip screen (Needed for every Screen function).

    -- Returns: void
end

Screen.debugPrint = function(x, y, text, color, screen_id, eye)
    -- Write a string on screen or image.

    -- x: int
    -- y: int
    -- text: string
    -- color: u32
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.refresh = function()
    -- Refresh screen buffers.

    -- Returns: void
end

Screen.clear = function(screen_id)
    -- Clear screen.

    -- screen_id: Image_id

    -- Returns: void
end

Screen.fillRect = function(x1, x2, y1, y2, color, screen_id, eye)
    -- Draw a rectangle.

    -- x1: int
    -- x2: int
    -- y1: int
    -- y2: int
    -- color: u32
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.fillEmptyRect = function(x1, x2, y1, y2, color, screen_id, eye)
    -- Draw a rectangular box.

    -- x1: int
    -- x2: int
    -- y1: int
    -- y2: int
    -- color: u32
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.drawLine = function(x1, x2, y1, y2, color, screen_id, eye)
    -- Draw a line.

    -- x1: int
    -- x2: int
    -- y1: int
    -- y2: int
    -- color: u32
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.drawPixel = function(x, y, color, screen_id, eye)
    -- Draw a pixel.

    -- x: int
    -- y: int
    -- color: u32
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.getPixel = function(x, y, screen_id, eye)
    -- Get the color of a pixel.

    -- x: int
    -- y: int
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: u32
end

Screen.loadImage = function(filename)
    -- Load a BMP, PNG or JPG image file.

    -- filename: string

    -- Returns: Image_id
end

Screen.drawImage = function(x, y, image, screen_id, eye)
    -- Draw an image.

    -- x: int
    -- y: int
    -- image: Image_id
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.drawPartialImage = function(x, y, image_x, image_y, width, height, image, screen_id, eye)
    -- Draw a part of an image.

    -- x: int
    -- y: int
    -- image_x: int
    -- image_y: int
    -- width: int
    -- height: int
    -- image: Image_id
    -- screen_id: Image_id
    -- eye: Eye_id]

    -- Returns: void
end

Screen.getImageWidth = function(image)
    -- Get width of an image.

    -- image: Image_id

    -- Returns: int
end

Screen.getImageHeight = function(image)
    -- Get height of an image.

    -- image: Image_id

    -- Returns: int
end

Screen.flipImage = function(source, destination)
    -- Flip an image.

    -- source: Image_id
    -- destination: Image_id

    -- Returns: void
end

Screen.createImage = function(width, height, color)
    -- Create an empty image.

    -- width: int
    -- height: int
    -- color: u32

    -- Returns: Image_id
end

Screen.freeImage = function(image)
    -- Delete an image from memory.

    -- image: Image_id

    -- Returns: void
end

Screen.saveImage = function(image, file, jpg_compression)
    -- Save an image as BMP/JPG file.

    -- image: Image_id
    -- file: string
    -- jpg_compression: bool

    -- Returns: void
end

Screen.enable3D = function()
    -- Enable stereoscopic 3D.

    -- Returns: void
end

Screen.disable3D = function()
    -- Disable stereoscopic 3D.

    -- Returns: void
end

Screen.get3DLevel = function()
    -- Get current stereoscopic 3D slide level.

    -- Returns: float
end

-- end of Screen Module





-- start of Graphics Module

local Graphics = {}

Graphics.init = function()
    -- Init GPU.

    -- Returns: void
end

Graphics.term = function()
    -- Term GPU.

    -- Returns: void
end

Graphics.initBlend = function(screen, side)
    -- Init GPU blending.

    -- screen: int
    -- side: int (optional)

    -- Returns: void
end

Graphics.termBlend = function()
    -- Term GPU blending.

    -- Returns: void
end

Graphics.loadImage = function(filename)
    -- Load an image as GPU texture.

    -- filename: string

    -- Returns: u32
end

Graphics.freeImage = function(texture)
    -- Free a GPU texture.

    -- texture: u32

    -- Returns: void
end

Graphics.flip = function()
    -- Swap GPU screens.

    -- Returns: void
end

Graphics.drawImage = function(x, y, texture, color)
    -- Blit a GPU texture.

    -- x: float
    -- y: float
    -- texture: int
    -- color: u32 (optional)

    -- Returns: void
end

Graphics.drawPartialImage = function(x, y, img_x, img_y, width, height, texture, color)
    -- Blit a part of a GPU texture.

    -- x: float
    -- y: float
    -- img_x: int
    -- img_y: int
    -- width: float
    -- height: float
    -- texture: int
    -- color: u32 (optional)

    -- Returns: void
end

Graphics.drawRotateImage = function(x, y, texture, radius, color)
    -- Blit a rotated GPU texture.

    -- x: float
    -- y: float
    -- texture: int
    -- radius: float
    -- color: u32 (optional)

    -- Returns: void
end

Graphics.drawScaleImage = function(x, y, texture, scale_x, scale_y, color)
    -- Blit a scaled GPU texture.

    -- x: float
    -- y: float
    -- texture: int
    -- scale_x: float
    -- scale_y: float
    -- color: u32 (optional)

    -- Returns: void
end

Graphics.drawImageExtended = function(x, y, img_x, img_y, width, height, radius, scale_x, scale_y, texture, color)
    -- Blit a GPU texture with custom settings.

    -- x: float
    -- y: float
    -- img_x: int
    -- img_y: int
    -- width: float
    -- height: float
    -- radius: float
    -- scale_x: float
    -- scale_y: float
    -- texture: int
    -- color: u32 (optional)

    -- Returns: void
end

Graphics.fillRect = function(x1, x2, y1, y2, color)
    -- Blit a rectangle using GPU.

    -- x1: float
    -- x2: float
    -- y1: float
    -- y2: float
    -- color: u32

    -- Returns: void
end

Graphics.fillEmptyRect = function(x1, x2, y1, y2, color)
    -- Blit an empty rectangle using GPU.

    -- x1: float
    -- x2: float
    -- y1: float
    -- y2: float
    -- color: u32

    -- Returns: void
end

Graphics.drawLine = function(x1, x2, y1, y2, color)
    -- Blit a line using GPU.

    -- x1: float
    -- x2: float
    -- y1: float
    -- y2: float
    -- color: u32

    -- Returns: void
end

Graphics.drawCircle = function(x1, x2, radius, color)
    -- Blit a circle using GPU.

    -- x1: float
    -- x2: float
    -- radius: int
    -- color: u32

    -- Returns: void
end

Graphics.getImageWidth = function(texture)
    -- Returns width of a loaded GPU texture.

    -- texture: int

    -- Returns: int
end

Graphics.getImageHeight = function(texture)
    -- Returns height of a loaded GPU texture.

    -- texture: int

    -- Returns: int
end

Graphics.convertFrom = function(image)
    -- Convert a CPU image to a GPU texture.

    -- image: int

    -- Returns: int
end

Graphics.getPixel = function(x, y, texture)
    -- Get the color of a pixel.

    -- x: int
    -- y: int
    -- texture: int

    -- Returns: u32
end

Graphics.setViewport = function(x, y, w, h, viewmode)
    -- Set GPU scene viewport.

    -- x: int
    -- y: int
    -- w: int
    -- h: int
    -- viewmode: view_id

    -- Returns: u32
end

-- end of Graphics Module





-- start of Console Module

local Console = {}

Console.new = function(screen)
    -- Create a new console.

    -- screen: Image_id

    -- Returns: u32
end

Console.clear = function(console)
    -- Clear a console.

    -- console: u32

    -- Returns: void
end

Console.show = function(console)
    -- Print content of a console.

    -- console: u32

    -- Returns: int
end

Console.append = function(console, text)
    -- Append a text to a console.

    -- console: u32
    -- text: string

    -- Returns: void
end

Console.destroy = function(console)
    -- Free a console.

    -- console: u32

    -- Returns: void
end

-- end of Console Module





-- start of Font Module

local Font = {}

Font.load = function(filename)
    -- Load a TTF font.

    -- filename: string

    -- Returns: u32
end

Font.setPixelSizes = function(font, size)
    -- Set size for a font.

    -- font: u32
    -- size: u32

    -- Returns: void
end

Font.print = function(font, x, y, text, color, screen, side)
    -- Print a text with a font.

    -- font: u32
    -- x: int
    -- y: int
    -- text: string
    -- color: u32
    -- screen: u32
    -- side: u32 (optional)

    -- Returns: void
end

Font.unload = function(font)
    -- Free a font.

    -- font: u32

    -- Returns: void
end

-- end of Font Module





-- start of Color Module

local Color = {}

Color.new = function(r, g, b, a)
    -- Create a new RGB or RGBA color.

    -- r: int
    -- g: int
    -- b: int
    -- a: int (optional)

    -- Returns: u32
end

Color.getR = function(color)
    -- Get R value of a color.

    -- color: u32

    -- Returns: int
end

Color.getG = function(color)
    -- Get G value of a color.

    -- color: u32

    -- Returns: int
end

Color.getB = function(color)
    -- Get B value of a color.

    -- color: u32

    -- Returns: int
end

Color.getA = function(color)
    -- Get A value of a color.

    -- color: u32

    -- Returns: int
end

Color.convertFrom = function(color)
    -- Convert a Render color to a Color ones.

    -- color: color_id

    -- Returns: int
end

-- end of Color Module





-- start of Controls Module

local Controls = {}

Controls.read = function()
    -- Read controls input.

    -- Returns: u32
end

Controls.check = function(controls, button)
    -- Check if a button is pressed.

    -- controls: u32
    -- button: u32

    -- Returns: bool
end

Controls.readCirclePad = function()
    -- Get X,Y position of Circle Pad.

    -- Returns: int,int
end

Controls.readCstickPad = function()
    -- Get X,Y position of Circle Pad Pro.

    -- Returns: int,int
end

Controls.readTouch = function()
    -- Get X,Y position of touchscreen pixel.

    -- Returns: int,int
end

Controls.getVolume = function()
    -- Get current console volume.

    -- Returns: int
end

Controls.headsetStatus = function()
    -- Check if headset is inserted.

    -- Returns: bool
end

Controls.shellStatus = function()
    -- Check if shell is opened.

    -- Returns: bool
end

Controls.enableScreen = function(screen)
    -- Power on a screen.

    -- screen: screen_id

    -- Returns: void
end

Controls.disableScreen = function(screen)
    -- Power off a screen.

    -- screen: screen_id

    -- Returns: void
end

Controls.enableGyro = function()
    -- Enable Gyroscope.

    -- Returns: void
end

Controls.disableGyro = function()
    -- Disable Gyroscope.

    -- Returns: void
end

Controls.enableAccel = function()
    -- Enable Accelerometer.

    -- Returns: void
end

Controls.disableAccel = function()
    -- Disable Accelerometer.

    -- Returns: void
end

Controls.readGyro = function()
    -- Reads Gyroscope status.

    -- Returns: int,int,int
end

Controls.readAccel = function()
    -- Reads Accelerometer status.

    -- Returns: int,int,int
end

-- end of Controls Module





-- start of Timer Module

local Timer = {}

Timer.new = function()
    -- Create a new timer.

    -- Returns: u32
end

Timer.getTime = function(timer)
    -- Returns time in milliseconds of timer object.

    -- timer: u32

    -- Returns: u32
end

Timer.destroy = function(timer)
    -- Free a timer object.

    -- timer: u32

    -- Returns: void
end

Timer.reset = function(timer)
    -- Reset time of a timer object.

    -- timer: u32

    -- Returns: void
end

Timer.pause = function(timer)
    -- Pause a timer object.

    -- timer: u32

    -- Returns: void
end

Timer.resume = function(timer)
    -- Resume a timer object.

    -- timer: u32

    -- Returns: void
end

Timer.isPlaying = function(timer)
    -- Check if a timer is paused or not.

    -- timer: u32

    -- Returns: bool
end

-- end of Timer Module





-- start of Network Module

local Network = {}

Network.isWifiEnabled = function()
    -- Check if WiFi is enabled.

    -- Returns: bool
end

Network.getMacAddress = function()
    -- Get console MAC address.

    -- Returns: string
end

Network.getIPAddress = function()
    -- Get LAN IP address.

    -- Returns: string
end

Network.updateFTP = function()
    -- Sets up an FTP server and get last received command.

    -- Returns: string
end

Network.downloadFile = function(url, filename, user_agent, method, post_data)
    -- Download a file via HTTP protocool.

    -- url: string
    -- filename: string
    -- user_agent: string (optional)
    -- method: u8 (optional)
    -- post_data: string (optional)

    -- Returns: void
end

Network.requestString = function(url, user_agent, method, post_data)
    -- Request a string via HTTP protocool.

    -- url: string
    -- user_agent: string (optional)
    -- method: u8 (optional)
    -- post_data: string (optional)

    -- Returns: string
end

Network.sendMail = function(to, subject, body)
    -- Send a mail via HTTP protocool.

    -- to: string
    -- subject: string
    -- body: string

    -- Returns: bool
end

Network.getWifiLevel = function()
    -- Return power level of your WiFi connection (1 to 3).

    -- Returns: int
end

Network.addCertificate = function(filename)
    -- Add a certificate to SSL context.

    -- filename: string

    -- Returns: int
end

-- end of Network Module





-- start of Socket Module

local Socket = {}

Socket.init = function()
    -- Init socketing system.

    -- Returns: void
end

Socket.term = function()
    -- Term socketing system.

    -- Returns: void
end

Socket.createServerSocket = function(port)
    -- Create a server socket.

    -- port: int

    -- Returns: int
end

Socket.connect = function(host, port, use_ssl)
    -- Create a client socket and connect it to the host.

    -- host: string
    -- port: int
    -- use_ssl: bool (optional)

    -- Returns: int
end

Socket.send = function(sock_id, text)
    -- Send a message.

    -- sock_id: int
    -- text: string

    -- Returns: void
end

Socket.receive = function(sock_id, size)
    -- Receive a message.

    -- sock_id: int
    -- size: int

    -- Returns: string
end

Socket.accept = function(sock_id)
    -- Search for connections (Server sockets).

    -- sock_id: int

    -- Returns: int
end

Socket.close = function(sock_id)
    -- Close a socket.

    -- sock_id: int

    -- Returns: void
end

-- end of Socket Module





-- start of Mic Module

local Mic = {}

Mic.start = function(seconds, samplerate)
    -- Start recording with microphone.

    -- seconds: int
    -- samplerate: int

    -- Returns: void
end

Mic.stop = function()
    -- Stop recording and gets recorded sound.

    -- Returns: wav_id
end

Mic.isRecording = function()
    -- Check if microphone is still recording.

    -- Returns: bool
end

Mic.pause = function()
    -- Pause microphone.

    -- Returns: void
end

Mic.resume = function()
    -- Resume microphone.

    -- Returns: void
end

-- end of Mic Module





-- start of Camera Module

local Camera = {}

Camera.init = function(screen, camera, photo_mode, is3D)
    -- Initialize camera.

    -- screen: screen_id
    -- camera: cam_id
    -- photo_mode: photo_id
    -- is3D: bool (optional)

    -- Returns: void
end

Camera.getOutput = function()
    -- Print camera output on screen.

    -- Returns: void
end

Camera.takePhoto = function(filename, resolution, jpg_compression)
    -- Take a photo with the camera.

    -- filename: string
    -- resolution: res_value
    -- jpg_compression: bool (optional)

    -- Returns: void
end

Camera.takeImage = function()
    -- Take an image with the camera.

    -- Returns: u32
end

Camera.term = function()
    -- Terminate camera.

    -- Returns: void
end

-- end of Camera Module





-- start of Sound Module

local Sound = {}

Sound.init = function()
    -- Initialize sound system.

    -- Returns: void
end

Sound.term = function()
    -- Terminate sound system.

    -- Returns: void
end

Sound.openWav = function(filename, use_streaming)
    -- Open a WAV file.

    -- filename: string
    -- use_streaming: bool (optional)

    -- Returns: wav_id
end

Sound.openAiff = function(filename, use_streaming)
    -- Open an AIFF/AIF file.

    -- filename: string
    -- use_streaming: bool (optional)

    -- Returns: wav_id
end

Sound.openOgg = function(filename, use_streaming)
    -- Open an OGG file.

    -- filename: string
    -- use_streaming: bool (optional)

    -- Returns: wav_id
end

Sound.updateStream = function()
    -- Update buffer for a sound stream.

    -- Returns: void
end

Sound.isPlaying = function(wav_file)
    -- Get sound playing state.

    -- wav_file: wav_id

    -- Returns: bool
end

Sound.play = function(wav_file, loop)
    -- Play a loaded sound.

    -- wav_file: wav_id
    -- loop: int

    -- Returns: void
end

Sound.close = function(wav_file)
    -- Close and free a loaded sound.

    -- wav_file: wav_id

    -- Returns: void
end

Sound.pause = function(wav_file)
    -- Pause a sound.

    -- wav_file: wav_id

    -- Returns: void
end

Sound.resume = function(wav_file)
    -- Resume a sound.

    -- wav_file: wav_id

    -- Returns: void
end

Sound.saveWav = function(wav_file, filename)
    -- Save a sound as WAV file.

    -- wav_file: wav_id
    -- filename: string

    -- Returns: void
end

Sound.getTotalTime = function(wav_file)
    -- Get Total Time of a sound.

    -- wav_file: wav_id

    -- Returns: int
end

Sound.getTime = function(wav_file)
    -- Get Current Time of a sound.

    -- wav_file: wav_id

    -- Returns: int
end

Sound.getSrate = function(wav_file)
    -- Get samplerate of a sound.

    -- wav_file: wav_id

    -- Returns: int
end

Sound.getTitle = function(wav_file)
    -- Get Title of a sound.

    -- wav_file: wav_id

    -- Returns: string
end

Sound.getAuthor = function(wav_file)
    -- Get Author of a sound.

    -- wav_file: wav_id

    -- Returns: string
end

Sound.getType = function(wav_file)
    -- Get audiotype of a sound. (1 = Mono, 2 = Stereo)

    -- wav_file: wav_id

    -- Returns: int
end

Sound.getService = function()
    -- Returns used audio service.

    -- Returns: string
end

-- end of Sound Module





-- start of JPGV Module

local JPGV = {}

JPGV.load = function(filename)
    -- Load a JPGV file.

    -- filename: string

    -- Returns: jpgv_id
end

JPGV.start = function(jpgv, loop)
    -- Start a JPGV video.

    -- jpgv: jpgv_id
    -- loop: int

    -- Returns: void
end

JPGV.draw = function(x, y, jpgv, screen, use3D)
    -- Draw JPGV current frame.

    -- x: int
    -- y: int
    -- jpgv: jpgv_id
    -- screen: int
    -- use3D: bool (optional)

    -- Returns: void
end

JPGV.drawFast = function(jpgv, screen, use3D)
    -- Draw fullscreen JPGV current frame in fast mode.

    -- jpgv: jpgv_id
    -- screen: int
    -- use3D: bool (optional)

    -- Returns: void
end

JPGV.unload = function(jpgv)
    -- Unload a JPGV file.

    -- jpgv: jpgv_id

    -- Returns: void
end

JPGV.getFPS = function(jpgv)
    -- Get JPGV framerate.

    -- jpgv: jpgv_id

    -- Returns: u32
end

JPGV.getFrame = function(jpgv)
    -- Get JPGV current frame number.

    -- jpgv: jpgv_id

    -- Returns: u32
end

JPGV.showFrame = function(x, y, jpgv, frame_number, screen, eye)
    -- Show a selected frame from JPGV file.

    -- x: int
    -- y: int
    -- jpgv: jpgv_id
    -- frame_number: int
    -- screen: int
    -- eye: Eye_Id]

    -- Returns: void
end

JPGV.getSrate = function(jpgv)
    -- Get JPGV audio samplerate.

    -- jpgv: jpgv_id

    -- Returns: u32
end

JPGV.getSize = function(jpgv)
    -- Get JPGV total frame number.

    -- jpgv: jpgv_id

    -- Returns: u32
end

JPGV.isPlaying = function(jpgv)
    -- Get JPGV playback state.

    -- jpgv: jpgv_id

    -- Returns: bool
end

JPGV.stop = function(jpgv)
    -- Stop a JPGV video.

    -- jpgv: jpgv_id

    -- Returns: void
end

JPGV.pause = function(jpgv)
    -- Pause a JPGV video.

    -- jpgv: jpgv_id

    -- Returns: void
end

JPGV.resume = function(jpgv)
    -- Resume a JPGV video.

    -- jpgv: jpgv_id

    -- Returns: void
end

-- end of JPGV Module





-- start of Render Module

local Render = {}

Render.init = function(w, h, color)
    -- Init 3D Rendering.

    -- w: int
    -- h: int
    -- color: u32

    -- Returns: void
end

Render.term = function()
    -- Term 3D Rendering.

    -- Returns: void
end

Render.createVertex = function(x, y, z, tex_x, tex_y, norm_x, norm_y, norm_z)
    -- Creates a vertex.

    -- x: float
    -- y: float
    -- z: float
    -- tex_x: float
    -- tex_y: float
    -- norm_x: float
    -- norm_y: float
    -- norm_z: float

    -- Returns: vertex_id
end

Render.loadModel = function(vertex_table, texture, ambient, diffuse, specular, shininess)
    -- Load a model from a vertex table.

    -- vertex_table: table
    -- texture: string
    -- ambient: color_id
    -- diffuse: color_id
    -- specular: color_id
    -- shininess: float

    -- Returns: u32
end

Render.loadObject = function(filename, texture, ambient, diffuse, specular, shininess)
    -- Load a model from a obj file.

    -- filename: string
    -- texture: string
    -- ambient: color_id
    -- diffuse: color_id
    -- specular: color_id
    -- shininess: float

    -- Returns: u32
end

Render.unloadModel = function(model_id)
    -- Free a loaded model.

    -- model_id: u32

    -- Returns: void
end

Render.initBlend = function(screen, eye)
    -- Init blending phase.

    -- screen: screen_id
    -- eye: eye_id (optional)

    -- Returns: void
end

Render.termBlend = function()
    -- Term blending phase.

    -- Returns: void
end

Render.drawModel = function(model_id, x, y, z, angle_x, angle_y)
    -- Draws a loaded model.

    -- model_id: u32
    -- x: float
    -- y: float
    -- z: float
    -- angle_x: float
    -- angle_y: float

    -- Returns: void
end

Render.createColor = function(r, g, b, a)
    -- Create a Render color.

    -- r: float
    -- g: float
    -- b: float
    -- a: float

    -- Returns: color_id
end

Render.convertColorFrom = function(color)
    -- Convert a Color module color to a Render ones.

    -- color: u32

    -- Returns: color_id
end

Render.setLightColor = function(color)
    -- Set vertex shader light color.

    -- color: color_id

    -- Returns: void
end

Render.setLightSource = function(x, y, z)
    -- Set vertex shader light position.

    -- x: float
    -- y: float
    -- z: float

    -- Returns: void
end

Render.useMaterial = function(model, ambient, diffuse, specular, shininess)
    -- Set material attributes for a loaded model.

    -- model: u32
    -- ambient: color_id
    -- diffuse: color_id
    -- specular: color_id
    -- shininess: float

    -- Returns: void
end

Render.useTexture = function(model, texture)
    -- Set a texture for a loaded model.

    -- model: u32
    -- texture: string

    -- Returns: void
end

-- end of Render Module





-- start of Keyboard Module

local Keyboard = {}

Keyboard.show = function()
    -- Shows keyboard on bottom screen.

    -- Returns: void
end

Keyboard.getState = function()
    -- Gets internal state of keyboard.

    -- Returns: u8
end

Keyboard.getInput = function()
    -- Gets user input from keyboard.

    -- Returns: string
end

Keyboard.clear = function()
    -- Clears keyboard user input.

    -- Returns: void
end

-- end of Keyboard Module





-- start of Core Module

local Core = {}

Core.checkService = function(service)
    -- Check if a service is accessible or not.

    -- service: string

    -- Returns: bool
end

Core.execCall = function(syscall, ...)
    -- Exec a C syscall.

    -- syscall: string

    -- Returns: int
end

Core.getHandle = function(handle)
    -- Get an lpp-3ds filehandle starting from a C filehandle.

    -- handle: u32

    -- Returns: u32
end

Core.readWord = function(offset)
    -- Read 4 bytes from specified offset.

    -- offset: u32

    -- Returns: u32
end

Core.storeWord = function(offset, word)
    -- Store 4 bytes to specified offset.

    -- offset: u32
    -- word: u32

    -- Returns: void
end

Core.alloc = function(size)
    -- Allocate a memory block.

    -- size: u32

    -- Returns: void
end

Core.linearAlloc = function(size)
    -- Allocate a memory block on linear heap.

    -- size: u32

    -- Returns: void
end

Core.free = function(offset)
    -- Frees an allocated memory block.

    -- offset: u32

    -- Returns: void
end

Core.linearFree = function(offset)
    -- Frees an allocated memory block on linear heap.

    -- offset: u32

    -- Returns: void
end

-- end of Core Module



