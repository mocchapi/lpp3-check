#!/usr/bin/python3

from pathlib import Path

root = Path(os.path.realpath(__file__)).parent


def luaify_function(function_in, description):
	return_type,*function_in = function_in.split(' ')
	if function_in[0] == 'u8':
		return_type += ' u8'
		del function_in[0]
	function_in = ' '.join(function_in)
	module_name,*function_in = function_in.split('.')
	function_in = '.'.join(function_in)
	function_name,rest = function_in.split('(')

	args = {}

	splitter = ''

	for item in rest.split(','):
		item = item.strip()
		if item.endswith(')'):
			item = item[:-1]
		if item == 'void' or item == '':
			continue
		*vtype,name = item.split(' ')
		if name.endswith(']'):
			name = name[:-1]
			vtype.append('(optional)')
		args[name] = ' '.join(vtype).replace('[','')

	dstrs = ''
	newline = '\n'
	for item in args:
		if args[item]:
			dstrs += f'{newline}    -- {item}: {args[item]}'
	if len(dstrs) > 0:
		dstrs += newline

	return f"{module_name}.{function_name} = function({', '.join(args.keys())}){newline}    -- {description}{newline}{dstrs}{newline}    -- Returns: {return_type}{newline}end"

def parse_html(api_text):
	out = {}
	current = None
	for line in api_text.split('\n'):
		if line.startswith('<tr bgcolor="#FFFF99"><td><center><font size=4><b>'):
			module_name = line.replace('<tr bgcolor="#FFFF99"><td><center><font size=4><b>', '').replace('</b></font></center></td></tr>','')
			if module_name == 'I/O Module':
				module_name = 'io Module'
			elif module_name == 'Video Module':
				module_name = 'JPGV Module'
			if not module_name in out:
				out[module_name] = {}
			current = module_name
		elif line.startswith('<font color=red><b>'):
			both = line.replace("<font color=red><b>",'').replace('<br>','')
			value,description = both.split('</b></font> - ')
			out[current][value] = description
	return out


def main():
	with open(root / 'api.html','r') as f:
		api_text = f.read()

	parsed_sections = parse_html(api_text)

	out = ['--\n-- Dummy functions & globals for static checking & such\n--']

	for key in parsed_sections:
		out.append(f'-- start of {key}')
		if key.endswith('Module'):
			out.append("local " + key.replace(' Module','')+' = {}')
		
		for value,description in parsed_sections[key].items():
			if '(' in value:
				out.append(luaify_function(value, description))
			else:
				newline = '\n'
				out.append(f"local {value} = 0{newline} -- {description}")
		out.append(f'-- end of {key}')
		out.append('')
		out.append('')

	out_str = '\n\n'.join(out)
	# print(out_str){newline}
	with open(root / 'api.lua','w') as f:
		f.write(out_str)

if __name__ == '__main__':
	main()