import os
from setuptools import setup

import lpp3check


license = ""
with open('./LICENSE') as f:
	license = f.read()

readme = ""
# with open('./README.md') as f:
# 	readme = f.read()

notice = """
  /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\
       lpp3-check requires the `luacheck` tool t to function!
     Look at https://github.com/mpeterv/luacheck for more info!
  /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\ /!\\
"""

print(notice)

setup(
	name = 'lpp3-check',
	description = "A utility for statically checking LPP-3DS lua code, using luacheck",
	long_description = readme,
	long_description_content_type = 'text/markdown',
	version = str(lpp3check.__VERSION__),
	url = str(lpp3check.__SOURCE__),
	license = license,

	packages = [ 'lpp3check' ],
	include_package_data = True,

	entry_points = {
		'console_scripts': [
			'lpp3-check = lpp3check:main',
			]
	},

	install_requires=[],

	python_requires = '>=3.10',
	classifiers = [
		"Programming Language :: Python :: 3",
		"Operating System :: OS Independent",
		"Environment :: Console",
		"License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
		"Topic :: Software Development",
	]
)

print(notice)